@profile
def memoryleak(n):
    data = []
    data.append(n)
    data.extend([n] * n)


if __name__ == '__main__':
    for i in range(10000):
        memoryleak(i)
