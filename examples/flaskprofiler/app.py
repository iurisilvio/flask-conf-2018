from flask import Flask
import flask_profiler

import fib

app = Flask(__name__)
app.config["flask_profiler"] = {
    "enabled": app.config["DEBUG"],
    "storage": {"engine": "mongodb"},
}

@app.route('/fib/')
def _fib(n=30):
    result = fib.fib(n)
    return str(result)

flask_profiler.init_app(app)
