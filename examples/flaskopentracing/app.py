import logging
import time
import os

from flask import current_app, Flask
from flask_opentracing import FlaskTracer
from jaeger_client import Config
import requests
from opentracing_instrumentation.client_hooks import install_all_patches

import fib
import fibcached

app = Flask(__name__)

log_level = logging.DEBUG
logging.getLogger('').handlers = []
logging.basicConfig(format='%(asctime)s %(message)s', level=log_level)
config = Config(
    config={
        'sampler': {
            'type': 'const', 'param': 1
        },
        'logging': True,
        'local_agent': {
            'reporting_host': 'localhost'
        }
    },
    service_name="flaskopentracing"
)
jaeger_tracer = config.initialize_tracer()
install_all_patches()
tracer = FlaskTracer(jaeger_tracer, True, app)


@app.route('/')
def index():
    return """
    <html><body>
    <ul>
        <li>
            <a href='/fib/'>fib</a>
        </li>
        <li>
            <a href='/fibcached/'>fibcached</a>
        </li>
    </ul>
    </body></html>
    """


@app.route('/fib/')
def _fib(n=30):
    result = fib.fib(n)
    return f"<html><body>{result}</body></html>"


@app.route('/fibcached/')
def _fibcached(n=30):
    result = fibcached.fib(n)
    return f"<html><body>{result}</body></html>"


@app.route('/pr/')
def pull_requests():
    # Fetch a list of pull requests on the opentracing repository
    github_url = "https://api.github.com/repos/opentracing/opentracing-python/pulls"
    import ipdb;ipdb.set_trace()
    r = requests.get(github_url)

    json = r.json()
    pull_request_titles = map(lambda item: item['title'], json)

    return 'OpenTracing Pull Requests: ' + ', '.join(pull_request_titles)


if __name__ == '__main__':
    app.run()
