import time

from flask import current_app, Flask, g, request
from werkzeug.contrib.profiler import ProfilerMiddleware

import fib
import fibcached

app = Flask(__name__)
app.wsgi_app = ProfilerMiddleware(app.wsgi_app)


@app.before_request
def before_request():
    request_start_time = time.time()
    g.request_time = lambda: "%.5fs" % (time.time() - request_start_time)


@app.after_request
def after_request(response):
    current_app.logger.info(f"{request.path} {g.request_time()}")
    return response


@app.route('/')
def index():
    return """
    <ul>
        <li>
            <a href='/fib/'>fib</a>
        </li>
        <li>
            <a href='/fibcached/'>fibcached</a>
        </li>
    </ul>
    """


@app.route('/fib/')
def _fib(n=30):
    return str(fib.fib(n))


@app.route('/fibcached/')
def _fibcached(n=30):
    return str(fibcached.fib(n))


if __name__ == '__main__':
    app.run()
