import time

from flask import Flask, g, request

import fib
import fibcached

app = Flask(__name__)


@app.before_request
def before_request():
    request_start_time = time.time()
    g.request_time = lambda: "%.5f" % (time.time() - request_start_time)


@app.after_request
def after_request(response):
    response.headers['X-App-Timing'] = g.request_time()
    return response


@app.route('/fib/')
def _fib(n=30):
    return str(fib.fib(n))


@app.route('/fibcached/')
def _fibcached(n=30):
    return str(fibcached.fib(n))


if __name__ == '__main__':
    app.run()
