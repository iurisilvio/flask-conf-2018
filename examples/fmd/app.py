import time

from flask import current_app, Flask
import flask_monitoringdashboard as dashboard

import fib
import fibcached

app = Flask(__name__)
# app.secret_key = 'SECRET'
dashboard.bind(app)


@app.route('/')
def index():
    return """
    <html><body>
    <ul>
        <li>
            <a href='/fib/'>fib</a>
        </li>
        <li>
            <a href='/fibcached/'>fibcached</a>
        </li>
    </ul>
    </body></html>
    """


@app.route('/fib/')
def _fib(n=30):
    result = fib.fib(n)
    return f"<html><body>{result}</body></html>"


@app.route('/fibcached/')
def _fibcached(n=30):
    result = fibcached.fib(n)
    return f"<html><body>{result}</body></html>"


if __name__ == '__main__':
    app.run()
