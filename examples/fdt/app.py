from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension

import fib

app = Flask(__name__)
app.secret_key = 'SECRET'
DebugToolbarExtension(app)


@app.route('/fib/')
def _fib(n=30):
    result = fib.fib(n)
    return f"<html><body>{result}</body></html>"


if __name__ == '__main__':
    app.run()
