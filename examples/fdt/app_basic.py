from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
import requests

import fib

app = Flask(__name__)
app.secret_key = 'SECRET'
toolbar = DebugToolbarExtension(app)

@app.route('/fib/')
def _fib(n=30):
    requests.post('http://httpbin.org/delay/1')
    result = fib.fib(n)
    result = 'OK'
    return f"<html><body>{result}</body></html>"

if __name__ == '__main__':
    app.run()
