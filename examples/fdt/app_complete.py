from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
from flask_debugtoolbar_lineprofilerpanel.profile import line_profile
import requests

import fib

app = Flask(__name__)
app.secret_key = 'SECRET'
toolbar = DebugToolbarExtension(app)
app.config['DEBUG_TB_PANELS'] = list(app.config['DEBUG_TB_PANELS']) + [
    'flask_debugtoolbar_lineprofilerpanel.panels.LineProfilerPanel',
    'flask_debugtoolbar_flamegraph.FlamegraphPanel',
]

@app.route('/fib/')
@line_profile
def _fib(n=30):
    requests.post('http://httpbin.org/delay/1')
    result = fib.fib(n)
    return f"<html><body>{result}</body></html>"

if __name__ == '__main__':
    app.run()
