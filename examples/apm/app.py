from elasticapm.contrib.flask import ElasticAPM
from flask import current_app, Flask

import fib
import fibcached

app = Flask(__name__)

app.config['ELASTIC_APM'] = {
  # Set required service name. Allowed characters:
  # a-z, A-Z, 0-9, -, _, and space
  'SERVICE_NAME': 'flask-conf-apm',

  # Use if APM Server requires a token
  'SECRET_TOKEN': '',

  # Set custom APM Server URL (default: http://localhost:8200)
  'SERVER_URL': 'http://apm.iurisilv.io',
}
app.debug = False
apm = ElasticAPM(app)
apm.client.config.debug = True


@app.route('/')
def index():
    return """
    <html><body>
    <ul>
        <li>
            <a href='/fib/'>fib</a>
        </li>
        <li>
            <a href='/fibcached/'>fibcached</a>
        </li>
    </ul>
    </body></html>
    """


@app.route('/fib/')
def _fib(n=30):
    result = fib.fib(n)
    return f"<html><body>{result}</body></html>"


@app.route('/fibcached/')
def _fibcached(n=30):
    result = fibcached.fib(n)
    return f"<html><body>{result}</body></html>"


if __name__ == '__main__':
    app.run()
